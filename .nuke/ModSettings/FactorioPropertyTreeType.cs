namespace ModSettings;

public enum FactorioPropertyTreeType {
    None = 0,
    Bool = 1,
    Number = 2,
    String = 3,
    List = 4,
    Dictionary = 5
}