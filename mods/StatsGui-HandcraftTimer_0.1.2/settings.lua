-- SPDX-License-Identifier: MPL-2.0

data:extend{{
	type = "bool-setting",
	name = "StatsGui-HandcraftTimer:show-sensor",
	setting_type = "runtime-per-user",
	default_value = true,
}}
