-- SPDX-License-Identifier: MPL-2.0
data:extend{{
    type = "bool-setting",
    name = "statsgui-ms-show-sensor",
    setting_type = "runtime-per-user",
    default_value = true,
    order = "aa"
}, {
    type = "bool-setting",
    name = "statsgui-ms-max-speed-player",
    setting_type = "runtime-per-user",
    default_value = true,
    order = "ba"
}, {
    type = "bool-setting",
    name = "statsgui-ms-max-speed-vehicle",
    setting_type = "runtime-per-user",
    default_value = false,
    order = "ca"
}, {
    type = "bool-setting",
    name = "statsgui-ms-jet-fuel",
    setting_type = "runtime-per-user",
    default_value = true,
    order = "da"
}}
