---@diagnostic disable

local migration = {}

function migration.global()
    global.nth_tick_events = {}
end

return migration
